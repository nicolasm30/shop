let objShopping = []
const initProduct = () => {
    fetch('/assets/json/products.json')
        .then(res => res.json())
        .then(({
            products
        }) => {
            products.map(product => {
                product.thumbnail = "/assets/images/product.jpeg"
            });
            renderProducts(products)
        })
}
const renderProducts = (products) => {
    let productsContainer = document.getElementById('shopProducts')
    products.forEach(({
        name,
        unit_price,
        stock,
        thumbnail
    }) => {
        const article = document.createElement('article')
        article.className = 'product'
        const thumbnailProduct = document.createElement('div')
        thumbnailProduct.className = 'product__thumbnail'
        const imgProduct = document.createElement('img')
        imgProduct.src = thumbnail
        const bodyProduct = document.createElement('div')
        bodyProduct.className = 'product__body'
        const actions = document.createElement('div')
        actions.className = 'product__body__actions'
        const qty = document.createElement('input')
        qty.type = 'number'
        const btn = document.createElement('button')
        btn.className = 'btn'
        btn.innerHTML = 'Add to cart'
        btn.addEventListener('click', (e) => {
            e.preventDefault()
            let qtyBuy = parseInt(qty.value)
            shopProduct(name, unit_price, stock, qtyBuy)
            qty.value = ''
        })
        article.appendChild(thumbnailProduct)
        thumbnailProduct.appendChild(imgProduct)
        article.appendChild(bodyProduct)
        bodyProduct.appendChild(actions)
        actions.appendChild(qty)
        actions.appendChild(btn)
        productsContainer.appendChild(article)
    });
}
const shopProduct = (name, unit_price, stock, qty) => {
    if(isNaN(qty)){
        messages("You have not entered any value in the amount")
    }else{
        if (qty <= stock) {
            let myShopping = {
                name,
                qty,
                unit_price,
                totalProduct: qty * unit_price
            }
            objShopping.push(myShopping)
            setMyShoping(objShopping)
        } else {
            messages(`We don't have the number of ${qty} for the product ${name}`)
        }
    }
}

const setMyShoping = (myShopping) => {
    if (Object.keys(myShopping).length > 0) {
        let containerCart = document.getElementById('shopCart')
        let createOrderBtn = document.getElementById('createOrder')
        let totalPrice = 0;
        //let order = {};
        document.body.classList.add('isProducts')
        createOrderBtn.disabled = false
        containerCart.innerHTML = ''
        myShopping.forEach(({
            name,
            qty,
            unit_price,
            totalProduct
        }) => {
            let row = document.createElement('div')
            row.className = 'shop__cart__row'
            let nProduct = document.createElement('div')
            let qProduct = document.createElement('div')
            let upProduct = document.createElement('div')
            let tProduct = document.createElement('div')
            nProduct.innerHTML = name
            qProduct.innerHTML = qty
            upProduct.innerHTML = `$${unit_price}`
            tProduct.innerHTML = `$${totalProduct} `
            row.appendChild(nProduct)
            row.appendChild(qProduct)
            row.appendChild(upProduct)
            row.appendChild(tProduct)
            containerCart.appendChild(row)
            totalPrice = totalPrice + totalProduct
        })
        updateTotalPrice(totalPrice)
    }
}

const updateTotalPrice = (totalPrice) => {
    let price = document.querySelector('.price')
    price.innerHTML = ''
    price.innerHTML = totalPrice
}

const messages = (msg) => {
    const page = document.body;
    let message = document.createElement('div')
    message.className = 'message'
    message.innerHTML = `
        <div class="message__close">
            x 
        </div>
        <div class="message__body">
            <p>
               ${msg}
            </p>
        </div>
    `;
    page.appendChild(message)
    closeMessage(message)
}

const closeMessage = (message) => {
    let closebtn = document.querySelector('.message__close')
    if(closebtn){
        closebtn.addEventListener('click', e => {
            message.remove()
        })
    }
}

const generateOrder = () => {
    let createOrderBtn = document.getElementById('createOrder')
    let shops;
    let arrShop = [];
    let order;
    if(createOrderBtn){
        createOrderBtn.addEventListener('click', e => {
            let products = Array.from(document.querySelectorAll('.shop__cart__row'))
            let totalPrice = document.querySelector('.totalPrice')
            products.forEach(product => {
                let property = product.querySelectorAll('div')
                shops = {
                    "name" : property[0].innerHTML,
                    "quantity" : property[1].innerHTML,
                    "unit price" : property[2].innerHTML,
                    "total product" : property[3].innerHTML,
                }
                arrShop.push(shops)

            })
            order = {
                "shops" : arrShop,
                "total price": totalPrice.innerText
            }
            let procesOrder = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(order));
            let download = document.createElement('a')
            download.href = procesOrder;
            download.setAttribute('download', "order.json")
            download.click()
        })
    }
}


export {
    initProduct,
    generateOrder
}