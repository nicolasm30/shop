import { initProduct, generateOrder } from "./assets/js/product.js";
document.addEventListener('DOMContentLoaded', e => {
    initProduct(), 
    generateOrder()
})